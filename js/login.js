//defining variables to show the messages befire logging in
let message = "in init";
let date = new Date();
let justDate = date.toDateString();
let time = date.toLocaleTimeString();

//creating and appending the paragraph to show "Today's Message"
let messagePTag = document.createElement("p");
messagePTag.innerHTML = "<strong>Today's Message: </strong>" + message;
let showMessage = document.querySelector(".weather-container");
showMessage.appendChild(messagePTag);

//creating and appending the paragraph to show the date
let datePTag = document.createElement("p");
datePTag.innerHTML = "<strong>Today's Date: </strong>" + justDate;
let showDate = document.querySelector(".weather-container");
showDate.appendChild(datePTag);

//creating and appending the paragraph to show the time
let timePTag = document.createElement("p");
timePTag.innerHTML = "<strong>Time Now: </strong>" + time;
let showTime = document.querySelector(".weather-container");
showTime.appendChild(timePTag);

//validating email and password input
function loginValidate(){
  //getting the value of textboxes
  let emailValue = document.getElementById("email").value;
  let passValue = document.getElementById("pass").value;

  //checking if both are empty then showing an error to fill in the form
  if(emailValue === "" && passValue ===""){
    let errorPTag1 = document.createElement("p");
    errorPTag1.innerHTML = "Error! Please complete the form!";
    errorPTag1.style.color = "#FF0000";
    errorPTag1.style.fontSize = "large";
    let showError = document.querySelector(".weather-container");
    showError.appendChild(errorPTag1);
    return false;

  //if email is empty and password is not, then show a message that email must be entered
  }else if(emailValue === "" && passValue != ""){
    let errorPTag2 = document.createElement("p");
    errorPTag2.innerHTML = "Email address must be filled in!";
    errorPTag2.style.color = "#FF0000";
    errorPTag2.style.fontSize = "large";
    let showError = document.querySelector(".weather-container");
    showError.appendChild(errorPTag2);
    return false;

  //if email is not empty and password is empty show a message to enter password and it should be more than 6 charachters
  }else if(emailValue != "" && passValue === ""){
    let errorPTag3 = document.createElement("p");
    errorPTag3.innerHTML = "Password length must be at least 6 charachters!";
    errorPTag3.style.color = "#FF0000";
    errorPTag3.style.fontSize = "large";
    let showError = document.querySelector(".weather-container");
    showError.appendChild(errorPTag3);
    return false;

  //if both values are correct then sen an ajax request and get the json
  }else if(emailValue === "test@gmail.com" && passValue === "123456"){
    //when logged in successfully the change the content of the today's message
    let textPTag = document.createElement("p");
    textPTag.innerHTML = "<strong>Weather in Montreal for the next 5 days!<strong>"
    let showText = document.querySelector(".weather-container");
    showText.prepend(textPTag);
    //requesting ajax
    $.ajax(
        {url:"http://dataservice.accuweather.com/forecasts/v1/daily/5day/56186?apikey=w1HGdts2q1sGGqgKnldIqjWjA2KXK2Ar%20"},
      ).done(function (forcast){
        //getting the json values we need in the array and assigning to variables
        for(let counter=0; counter<5; counter++){
          let link = forcast.DailyForecasts[counter].Link;
          let dailyDate = "<a href =" + link + ">" + forcast.DailyForecasts[counter].Date +"</a>";
          let dailyMax = forcast.DailyForecasts[counter].Temperature.Maximum.Value;
          let dailyMin = forcast.DailyForecasts[counter].Temperature.Minimum.Value;
          let dayState = forcast.DailyForecasts[counter].Day.IconPhrase;
          let nightState = forcast.DailyForecasts[counter].Night.IconPhrase;

          //creating "li" tag and put variables into it to show in the DOM
          dailyForcast = document.createElement("li");
          dailyForcast.innerHTML = dailyDate + "<br>" + "Max: " + dailyMax + "F" + " Min: " + dailyMin + "F" + "<br>" + "Day: " + dayState + " Night: " + nightState;
          document.getElementById("weather-list").appendChild(dailyForcast);
        }
        messagePTag.innerHTML = "<strong>Today's Message: </strong>" + forcast.Headline.Text;
    });
  //if both textfields are filled with invalid data then show a message in the DOM that they are not valid
  }else{
    let errorPTag4 = document.createElement("p");
    errorPTag4.innerHTML = "Email or Password are not valid!!";
    errorPTag4.style.color = "#FF0000";
    errorPTag4.style.fontSize = "large";
    let showError = document.querySelector(".weather-container");
    showError.appendChild(errorPTag4);
    return false;
  }
}
